<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['auth:api'] ], function ()
{
    Route::get('/servidores', 'ApiServerController@all');
    Route::post('/servidores', 'ApiServerController@create');
    Route::patch('/servidores/{server}', 'ApiServerController@update');
    Route::delete('/servidores/{server}', 'ApiServerController@destroy');
    Route::get('/servidores/{server}', 'ApiServerController@allByServer');

    Route::get('/grupos', 'ApiGroupController@all');
    Route::post('/grupos/nuevo', 'ApiGroupController@create');
    Route::patch('/grupos/editar/{group}', 'ApiGroupController@update');
    Route::delete('/grupos/eliminar/{group}', 'ApiGroupController@delete');

    Route::get('/archivos/{file}', 'ApiFileController@getFiles');

    Route::post('/archivos/directorio', 'ApiFileController@createDirectory');
    Route::post('/archivos/archivo', 'ApiFileController@createFile');


    Route::get('/usuarios', 'ApiUserGroupController@getAllUsers');

    Route::post('/usuarios/grupos/nuevo', 'ApiGroupController@createUser');


    Route::get('/grupos/usuarios/{group}', 'ApiUserGroupController@getUsersByGroup');

});