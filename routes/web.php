<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout' );
Route::post('/login', 'Auth\LoginController@login');

Route::get('/grupos', 'GroupController@index');
Route::get('/permisos', 'GroupController@permission');
Route::get('/servidores', 'ServerController@index');
Route::get('/servidores/nuevo', 'ServerController@create');
Route::get('/servidores/editar/{server}', 'ServerController@update');
Route::get('/servidores/ver/{server}', 'ServerController@show');
Route::get('/archivos', 'FileController@index');
Route::get('/archivos/ver/{file}', 'FileController@show');
Route::get('/archivos/visualizar/{file}', 'FileController@visuality');