
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */
import VueCharts from 'vue-chartjs'
import { Bar, Line } from 'vue-chartjs'
import VeeValidate from 'vee-validate';

require('./bootstrap');

window.Vue = require('vue');

// Librería que ayuda a la validación de formularios
Vue.use(VeeValidate);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

//Adding poll pages
require('./views');

Vue.component('example-component', require('./components/ExampleComponent.vue'));

const app = new Vue({
    el: '#app'
});
