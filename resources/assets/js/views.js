Vue.component('vue-chart', require('./components/Chart.vue'));

Vue.component('server-index', require('./views/servers/PageServers.vue'));
Vue.component('server-create', require('./views/servers/PageServerCreate.vue'));

Vue.component('groups-index', require('./views/groups/PageGroups.vue'));

Vue.component('permissions', require('./views/permissions/PagePermissions.vue'));

Vue.component('files', require('./views/files/PageFiles.vue'));


Vue.component('folder-modal', require('./components/files/FolderModal.vue'));
Vue.component('file-modal', require('./components/files/FileModal.vue'));