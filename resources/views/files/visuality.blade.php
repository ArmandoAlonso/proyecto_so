@extends('layouts.master')


@section('content')


    <div>
        <!-- BEGIN EXAMPLE TABLE PORTLET-->
        <div class="portlet light portlet-fit ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-list font-blue"></i>
                    <span class="caption-subject font-blue sbold uppercase">{{ $file->name }}.{{ $file->ext }} ( {{ $file->size }} kb)</span>
                </div>
            </div>
            <div class="portlet-body">
                <div class="row">
                    {{ $file->content }}
                </div>

            </div>
        </div>

    </div>


@endsection

@section('scripts')

@endsection

