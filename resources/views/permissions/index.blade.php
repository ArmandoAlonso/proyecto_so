@extends('layouts.master')


@section('content')


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-list font-blue"></i>
                <span class="caption-subject font-blue sbold uppercase">Permisos</span>
            </div>
            <div class="actions">
                <a href="" class="btn btn-primary">
                    <i class="fa fa-plus"></i>
                    Acción
                </a>
            </div>
        </div>
        <div class="portlet-body">
            <permissions></permissions>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->


@endsection

@section('scripts')

@endsection

