@extends('layouts.master')


@section('content')


    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-list font-blue"></i>
                <span class="caption-subject font-blue sbold uppercase">
                    Bienvenido {{ Auth::user()->name }} ( {{ Auth::user()->password}})
                </span>

            </div>
        </div>
        <div class="portlet-body">
            @if( Auth::user()->permission_id == 1 )
                Por ser usuario administrador disfruta de los beneficios de administrar archivos y ver donde
                se encuentran alojados tus archivos.
            @else
                Por ser usuario cliente disfruta los beneficios de ver los archivos que te comparte tu administrador.
            @endif
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->


@endsection

@section('scripts')

@endsection

