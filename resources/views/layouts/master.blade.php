<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link rel="stylesheet" href="/css/global-mandatory.css">
    <link rel="stylesheet" href="/css/own-styles.css">
    <link rel="stylesheet" href="/plugins/css/select2.min.css">
    @yield('style')
    <link rel="stylesheet" href="/css/global-styles.css">


</head>

<!-- Global site tag (gtag.js) - Google Analytics -->
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

@include('layouts.header')

<div class="page-container" id="app">

    @include('layouts.sidebar')

    <div class="page-content-wrapper">
        <div class="page-content">
            @yield('content')
        </div>
    </div>

</div>

@include('layouts.footer')

<script src="{{ asset('/js/core-plugins.js') }}"></script>

<script src="{{ asset('/js/layout-scripts.js') }}"></script>
<script src="/plugins/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
<script src="https://unpkg.com/vue-chartjs/dist/vue-chartjs.min.js"></script>
@yield('scripts')
<script src="/js/app.js"></script>


</body>
</html>