@extends('layouts.master')


@section('content')

    <!-- BEGIN EXAMPLE TABLE PORTLET-->
    <div class="portlet light portlet-fit ">
        <div class="portlet-title">
            <div class="caption">
                <i class="icon-list font-blue"></i>
                <span class="caption-subject font-blue sbold uppercase">Detalle del servidor</span>
            </div>
            <div class="actions">

            </div>
        </div>
        <div class="portlet-body">
            <div class="container">
                <h3 class="font-blue-madison bold">Desglose</h3>
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>Nombre</th>
                        <th>Posición inicial</th>
                        <th>Posición final</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php $atMomenth = 0; ?>

                    @foreach( $server->files as $file)
                        @if (!$file->is_folder )
                            <tr>
                                <td>{{ $file->name }}.{{ $file->ext }}</td>
                                <td>
                                    {{ $atMomenth }} kb
                                </td>
                                <?php $atMomenth += $file->size;?>
                                <td>
                                    {{ $atMomenth }} kb
                                </td>
                            </tr>
                        @endif
                    @endforeach

                    <tr>
                        <td> LIBRE </td>
                        <td> {{ $atMomenth }} kb</td>
                        <td> {{ $server->size }} kb </td>
                    </tr>

                    </tbody>
                </table>
                <span class="pull-right">
                    Total libres: {{ $server->getRemaining() }} kb
                </span>

            </div>
        </div>
    </div>
    <!-- END EXAMPLE TABLE PORTLET-->

@endsection

@section('scripts')

@endsection

