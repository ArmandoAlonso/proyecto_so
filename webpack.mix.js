let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')

    .styles([
        'http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all',
        'resources/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'resources/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'resources/assets/global/plugins/select2/css/select2.min.css',
        'resources/assets/global/plugins/select2/css/select2-bootstrap.min.css',
        'resources/assets/global/css/components.min.css',
        'resources/assets/global/css/plugins.min.css',
        'resources/assets/pages/css/login-2.min.css',
    ],'./public/css/loguin.css')


    // Global Mandatory
    .styles([
        'resources/assets/global/plugins/font-awesome/css/font-awesome.min.css',
        'resources/assets/global/plugins/simple-line-icons/simple-line-icons.min.css',
        'resources/assets/global/plugins/bootstrap/css/bootstrap.min.css',
        'resources/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css',
        'resources/assets/global/plugins/bootstrap-sweetalert/sweetalert.css'
    ],'./public/css/global-mandatory.css')

    // Global styles
    .styles([
        'resources/assets/global/css/components.min.css',
        'resources/assets/global/css/plugins.min.css',
        'resources/assets/layouts/layout2/css/layout.min.css',
        'resources/assets/layouts/layout2/css/themes/blue.min.css',
        'resources/assets/layouts/layout2/css/custom.min.css'
    ],'./public/css/global-styles.css')

    //Layouts styles
    .styles( [
        'resources/assets/layouts/layout2/css/layout.min.css',
        'resources/assets/layouts/layout2/css/themes/blue.min.css',
        'resources/assets/layouts/layout2/css/custom.min.css',
    ], './public/css/theme-layout-styles.css')


    // Core scripts
    .scripts([
        'resources/assets/global/plugins/jquery.min.js',
        'resources/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'resources/assets/global/plugins/js.cookie.min.js',
        'resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'resources/assets/global/plugins/jquery.blockui.min.js',
        'resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'resources/assets/global/plugins/bootstrap-sweetalert/sweetalert.min.js',
        'resources/assets/pages/scripts/ui-sweetalert.min.js'
    ],'./public/js/core-plugins.js')

    // Layout scripts
    .scripts([
        'resources/assets/global/scripts/app.min.js',
        'resources/assets/layouts/layout2/scripts/layout.min.js',
        'resources/assets/layouts/layout2/scripts/demo.min.js',
        'resources/assets/layouts/global/scripts/quick-sidebar.min.js',
        'resources/assets/layouts/global/scripts/quick-nav.min.js'
    ],'./public/js/layout-scripts.js')


    .scripts([
        'resources/assets/global/plugins/jquery.min.js',
        'resources/assets/global/plugins/bootstrap/js/bootstrap.min.js',
        'resources/assets/global/plugins/js.cookie.min.js',
        'resources/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js',
        'resources/assets/global/plugins/jquery.blockui.min.js',
        'resources/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js',
        'resources/assets/global/plugins/jquery-validation/js/jquery.validate.min.js',
        'resources/assets/global/plugins/jquery-validation/js/additional-methods.min.js',
        'resources/assets/global/plugins/select2/js/select2.full.min.js',
        'resources/assets/global/scripts/app.min.js',
        'resources/assets/pages/scripts/login.min.js',
    ],'./public/js/loguin.js')


    .sass('resources/assets/sass/own-styles.scss', 'public/css')

    .sass('resources/assets/sass/app.scss', 'public/css');

mix.browserSync({
    proxy: 'proyecto_so.sw',
    open: false,
    notify: false
});