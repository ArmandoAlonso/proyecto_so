<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Server extends Model
{

    protected $fillable = [
        'name', 'size', 'location'
    ];

    public function files()
    {
        return $this->hasMany('App\Models\File');
    }


    public function getSizeAtMoment()
    {
        $files = $this->files;
        $size = 0;
        foreach ($files as $file)
        {
            if(!$file->is_folder)
                $size+= $file->size;
        }
        return $size;
    }

    public function getRemaining()
    {
        return $this->size - $this->getSizeAtMoment();
    }

    public static function hasSpace($size)
    {
        $servers = Server::all();

        $min = 100000000000;
        $minSever = null;
        foreach ( $servers as $server )
        {
            $remaining = $server->getRemaining();
            if( $remaining <= $min && $remaining >= $size)
                $minSever = $server;
        }
        return ($minSever != null) ? $minSever : null;
    }


}
