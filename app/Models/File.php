<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $fillable = [
        'name', 'content', 'ext', 'size',
        'server_id', 'is_folder', 'is_root',
        'file_id'
    ];

    public function files()
    {
        return $this->hasMany('App\Models\File');
    }
}
