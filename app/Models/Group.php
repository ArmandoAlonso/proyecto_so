<?php

namespace App\Model;

use App\Models\UserGroup;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = [
        'name', 'year', 'school','create', 'read',
        'update', 'delete'
    ];


    public function users()
    {
        $userGroup = UserGroup::where('group_id', $this->id)->get();
        $data = [];
        foreach ( $userGroup as $group)
        {
            $data[] = User::find($group->user_id);
        }
        return $data;
    }
}
