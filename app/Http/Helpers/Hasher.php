<?php

namespace App\Http\Helpers;

class Hasher
{
    public static function generateHash( $text )
    {
        // obtenemos el tamaño de la palabra
        $size = strlen($text);

        if( $size == 0)
            return "";

        //variable para obtener la suma
        $value = 0;

        //variable temporal para la primera letra
        $temp = ord($text[0]);

        //hacemos la suma ascii de todas
        for ($i = 1; $i < $size; $i++) {
            $temp += ord($text[$i]);
        }

        // iteramos sobre cada letra
        for ($i = 0; $i < strlen($text); $i++) {

            // restamos al temporal
            $temp-=ord($text[$i]);

            // multiplicamos el temporal mas el tamaño
            $value += ord($text[$i]) * ($size + $temp);

            // multiplicamos el tamaño de la palabra por 2
            $size = $size*2;
        }

        return $value . "";
    }
}
