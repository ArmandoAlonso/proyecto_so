<?php

namespace App\Http\Controllers;


class GroupController extends Controller
{
    public function index()
    {
        return view('groups.index');
    }

    public function permission()
    {
        return view('permissions.index');
    }
}
