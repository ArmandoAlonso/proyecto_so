<?php

namespace App\Http\Controllers\Auth;

use App\Http\Helpers\Hasher;
use App\Models\Permission;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        if(count(Permission::all()) == 0)
        {
            Permission::create(['name' => 'admin', 'level' => 1]);
            Permission::create(['name' => 'client', 'level' => 2]);
        }

        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        if( count(User::all()) == 0)
            $permission = Permission::where('level', 1)->first();
        else
            $permission = Permission::where('level', 2)->first();
        
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hasher::generateHash($data['password']),
            'permission_id' => $permission->id
        ]);
    }
}
