<?php

namespace App\Http\Controllers;

use App\Model\Group;
use App\Models\File;
use App\Models\UserGroup;
use Illuminate\Http\Request;

class ApiGroupController extends Controller
{
    public function all()
    {
        $groups = Group::all();
        return response($groups);
    }

    public function create(Request $request)
    {
        $group = Group::create($request->all());
        return response($group);
    }

    public function update(Request $request, Group $group)
    {
        $group->update($request->all());
        return response($group);
    }

    public function delete(Request $request, Group $group)
    {
        $group->delete();
        return response('Grupo eliminado.');
    }

    public function createUser(Request $request)
    {
        UserGroup::create($request->all());
        return response('Usuario asignado exitósamente');
    }


}
