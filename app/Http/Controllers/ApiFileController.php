<?php

namespace App\Http\Controllers;

use App\Http\Requests\ValidateFile;
use App\Models\File;
use App\Models\Server;
use Illuminate\Http\Request;

class ApiFileController extends Controller
{
    public function getFiles(File $file)
    {
        $files = $this->getData($file->files);
        return response([ 'files' => $files, 'father' => $file]);
    }


    public function createDirectory(Request $request)
    {
        $request['size'] = 1;
        $request['content'] = '';
        $request['ext'] = '';
        $request['is_folder'] = 1;
        $server = Server::hasSpace($request['size']);
        if($server == null)
                return response('No hay servidores disponibles.', 500);

        $request['server_id'] = $server->id;
        File::create($request->all());

        return response('Carpeta creada exitósamente.');
    }

    public function createFile(ValidateFile $request)
    {
        $request['is_folder'] = 0;
        $request['size'] = strlen($request->all()['content']) * 10;
        $server = Server::hasSpace($request['size']);
        if($server == null)
                return response('No hay servidores disponibles.', 500);

        $request['server_id'] = $server->id;
        File::create($request->all());

        return response('Archivo creado exitósamente.');
    }
}
