<?php

namespace App\Http\Controllers;

use App\Models\Server;
use Illuminate\Http\Request;

class ApiServerController extends Controller
{
    public function all()
    {
        $servers = Server::all();
        $servers = $this->getSize($servers);
        return response($servers);
    }

    public function create(Request $request)
    {
        Server::create($request->all());
        return response('Servidor creado exitósamente');
    }

    public function update(Server $server, Request $request)
    {
        $server->update($request->all());
        return response('Servidor modificaco exitósamente');
    }

    public function destroy(Server $server, Request $request)
    {
        $server->delete();
        return response('Servidor eliminado exitósamente');
    }

    public function allByServer(Server $server)
    {
        $files = $server->files;
        return response($files);
    }

    private function getSize($servers)
    {
        $data = [];
        foreach ($servers as $server)
        {
            $server['used'] = $server->getSizeAtMoment();
            $data[] = $server;
        }
        return $data;
    }
}
