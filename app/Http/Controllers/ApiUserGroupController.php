<?php

namespace App\Http\Controllers;

use App\Model\Group;
use App\User;
use Illuminate\Http\Request;

class ApiUserGroupController extends Controller
{
    public function getAllUsers()
    {
        $users = User::all();
        return response($users);
    }

    public function getUsersByGroup(Group $group)
    {
        return response($group->users());
    }
}
