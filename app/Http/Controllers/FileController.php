<?php

namespace App\Http\Controllers;


use App\Models\File;
use App\Models\Server;

class FileController extends Controller
{
    public function index()
    {
        $file = File::where('is_root', 1)->first();
        if($file == null)
        {
            $server = Server::hasSpace(1);
            if($server == null)
                return redirect('/servidores');

            $file = new File();
            $file->name = 'root';
            $file->size = 1;
            $file->content = '';
            $file->ext = '';
            $file->server_id = $server->id;
            $file->is_folder = 1;
            $file->is_root = 1;
            $file->save();
        }
        $files = $file->files;
            return view('files.index', compact('file', 'files'));
    }

    public function show(File $file)
    {
        $files = $file->files;
        return view('files.index', compact('file', 'files'));
    }

    public function visuality(File $file)
    {
        return view('files.visuality', compact('file'));
    }
}
