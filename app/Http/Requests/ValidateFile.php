<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidateFile extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $invalidFiles = [
            'exe' => [
                'update',
                'reading'
            ],
            'xls'=> [
                'getdata'
            ],
            'crypt' => [

            ]
        ];

        if(! isset($invalidFiles[$this->ext]))
            return true;

        if(count($invalidFiles[$this->ext]) == 0)
            return false;

        foreach ($invalidFiles[$this->ext] as $invalidFile)
        {
            if($invalidFile === $this->name)
                return false;
        }

        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            //
        ];
    }
}
